Notes on how this package can be tested.
────────────────────────────────────────

This package can be tested by executing

    sh installation-test
    sh non-default-params-test

in order to confirm its integrity.

To test manually, cd to some dir where you have write permissions
and do

    cp -a /usr/share/doc/conservation-code/examples/* .
    ln -s 1G6C_A_jsd.scores conservation_data/1G6C_C_jsd.scores
    ln -s 1G6C_B_jsd.scores conservation_data/1G6C_D_jsd.scores
    score_conservation -o alignment.scores 2plc__hssp-filtered.aln

Algorithm uses BLOSUM62 [1] alignment matrix, located at 
/usr/share/conservation-code/matrix/blosum62.bla.
If you want to use specify other alignment matrix, you can do it like so:

    ln -s /usr/share/conservation-code/matrix/blosum100.bla blosum100.bla
    score_conservation -m blosum100.bla -o alignment.scores 2plc__hssp-filtered.aln

For other parameters and their usage type "man score_conservation" in terminal.

Program tries to get some results even when numeric parameters are incorrect or
some data can't be found. If you provide matrix file which doesn't exist, it shows 
similar message:

  Could not load similarity matrix: /usr/share/conservation-code/matrix/blosum63.bla. Using identity matrix...

Identity matrix [2] makes everything work, but sequence alignment will look 
sparser than you expect when using blosum matrix.

If you provide incorrect distribution file, `score_conservation` shows similar 
message:

  [Errno 2] No such file or directory: '/usr/share/conservation-code/distributions/swissprot1.distribution' Using default (BLOSUM62) background.

These messages are written to stderr, but they simply inform you that your set
of parameters differs from those used in actual data processing. 

In contrast, if you see message with "Error:" text [2], it means that some
unprocessed errors appeared during program call and resulting file probably 
will not contain any sensible results.

References:

  [1]: BLOSUM alignment matrices https://en.wikipedia.org/wiki/BLOSUM
  [1]: https://en.wikipedia.org/wiki/Identity_matrix
  [3]: https://docs.python.org/2/tutorial/errors.html
