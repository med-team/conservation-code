=pod

=head1 NAME

score_conservation - score protein sequence conservation

=head1 SYNOPSIS

score_conservation [options] ALIGNFILE

=head1 DESCRIPTION

Score protein sequence conservation in B<ALIGNFILE>.  B<ALIGNFILE> must be in FASTA, CLUSTAL or Stockholm format.

The following conservation scoring methods are implemented:
 * sum of pairs
 * weighted sum of pairs
 * Shannon entropy
 * Shannon entropy with property groupings (Mirny and Shakhnovich 1995,
   Valdar and Thornton 2001)
 * relative entropy with property groupings (Williamson 1995)
 * von Neumann entropy (Caffrey et al 2004)
 * relative entropy (Samudrala and Wang 2006)
 * Jensen-Shannon divergence (Capra and Singh 2007)

A window-based extension that incorporates the estimated conservation of
sequentially adjacent residues into the score for each column is also given.
This window approach can be applied to any of the conservation scoring
methods.

With default parameters score_conservation(1) computes the conservation scores for the alignment using the
Jensen-Shannon divergence and a window B<-w> of I<3>.

The sequence-specific output can be used as the conservation input for
concavity(1).

Conservation is highly predictive in identifying catalytic sites and
residues near bound ligands.

=head1 REFERENCES

=over

=item Capra JA and Singh M. Predicting functionally important residues from sequence conservation. Bioinformatics, 23(15):1875-82, 2007.

=back

=head1 OPTIONS

=over

=item -a [NAME]

Reference sequence. Print scores in reference to the named sequence (ignoring gaps). Default prints the entire column.

=item -b [0-1]

Lambda for window heuristic linear combination. Default=I<.5>.

Equation:

C<score = (1 - lambda) * average_score_over_window_around_middle + lambda * score_of_middle>

=item -d [FILE]

Background distribution file, e.g. F<distributions/swissprot.distribution>. Default=built-in BLOSUM62.

=item -g [0-1)]

Gap cutoff. Do not score columns that contain more than gap cutoff fraction gaps. Default=I<.3>.

=item -h

Print help.

=item -l [true|false]

Use sequence weighting. Default=I<true>.

=item -m [FILE]

Similarity matrix file, e.g. F<matrix/blosum62.bla> or .qij. Default=F<matrix/blosum62.bla>.

Some methods, e.g. I<js_divergence>, do not use this.

=item -n [true|false]

Normalize scores. Print the z-score (over the alignment) of each column raw score. Default=I<false>.

=item -o FILE

Output file. Default: standard output stream.

=item -p [true|false]

Use gap penalty. Lower the score of columns that contain gaps, proportionally to the sum weight of the gapped sequences. Default=I<true>.

=item -s [METHOD]

Conservation estimation method, one of I<shannon_entropy property_entropy property_relative_entropy vn_entropy relative_entropy js_divergence sum_of_pairs>. Default=I<js_divergence>.

=item -w [0-INT]

Window size. Number of residues on either side included in the window. Default=I<3>.

=back

=head1 EXAMPLES

Note: you may have to copy and uncompress the example data files before running the following examples.

=over

=item Compute conservation scores for the alignment using the Jensen-Shannon divergence with default settings and print out the scores:

 score_conservation __docdir__/examples/2plc__hssp-filtered.aln

=item Score an alignment using Jensen-Shannon divergence, a window of size 3 (on either side of the residue), and the swissprot background distribution:

 score_conservation -s js_divergence -w 3 -d \
  __pkgdatadir__/distributions/swissprot.distribution \
  __docdir__/examples/2plc__hssp-filtered.aln

=back

=head1 FILES

=over

=item Distributions

F<__pkgdatadir__/distributions>

=item Matrices

F<__pkgdatadir__/matrix>

=back

=head1 SEE ALSO

=over

=item Homepage L<http://compbio.cs.princeton.edu/conservation/>

=item Publication L<http://bioinformatics.oxfordjournals.org/cgi/content/full/23/15/1875>

=item concavity(1)

=back

=cut
